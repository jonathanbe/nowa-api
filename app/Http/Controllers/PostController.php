<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ExampleController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Paginate resource.
     *
     * @return void
     */
    public function index(Request $request)
    {
        return response(['test', 'test2', 'test3'], 200);
    }
}
